d3 = require 'd3'
throttle = require './throttle'
cleanCategoryName = require './cleanCategoryName'
interaction = require './interaction'
d3Tip = require './vendor/index.js'

margin = { t: 20, r: 40, b: 60, l: 40 }
x = d3.scale.linear()
y = d3.scale.linear()
z = d3.scale.linear()
plotDiv = document.getElementById('plot-wrapper')
initWidth = 900
initHeight = 500
formatCurrency = d3.format '$,'
formatThousands = d3.format ','
ios = interaction.ios

xAxis = d3.svg.axis()
  .tickFormat d3.format '%'
  .ticks 5
  .orient 'bottom'

yAxis = d3.svg.axis()
  .ticks 5
  .tickFormat((d) ->
    val = d.toString()
    if val.length > 1
      val.slice(0, -3) + 'k'
    else
      val
  )
  .orient 'left'

tooltip = d3.tip()
  .offset [-10, 0]
  .attr
    class: 'tooltip'
  .html (d) ->
    if d.is2011 is '0'
      jobTitle = d.job_title
      d3.select '.footnote'
        .classed 'active-footnote', false
    else
      d3.select '.footnote'
        .classed 'active-footnote', true

      jobTitle = d.job_title + '*'

    "<h4>#{jobTitle}</h4>
    <p>Median wage: #{formatCurrency d.annual_median}</p>
    <p>Total employed: #{formatThousands d.total_employed}</p>"

mouseOn = (d) ->
  console.log ios
  if not ios
    tooltip.show(d)
  else
    tooltip.show(d)
    d3tooltip = d3.select('.tooltip')
    offsetTop = +d3tooltip.style('top').slice(0, -2) - document.body.scrollTop
    d3tooltip.style('top', offsetTop + 'px')

mouseOff = () ->
  d3.select '.footnote'
    .classed 'active-footnote', false
  tooltip.hide()

module.exports = (csv) ->

  maxAnnualMedian = d3.max csv, (d) -> d.annual_median
  extentTotalEmployed = d3.extent csv, (d) -> d.total_employed

  x.domain [0, 1]
  y.domain [0, maxAnnualMedian]
  z.domain extentTotalEmployed
  
  xAxis.scale x
  yAxis.scale y

  plotSvg = d3.select '#plot-wrapper'
    .append 'svg'
    .attr
      class: 'plot-svg'
      width: initWidth
      height: initHeight


  plotG = plotSvg.append 'g'
    .attr
      class: 'plot-g'
      transform: "translate(#{[margin.l, margin.t]})"

  xAxisSvg = plotG.append 'g'
    .attr
      class: 'x axis'

  yAxisSvg = plotG.append 'g'
    .attr
      class: 'y axis'

  xLabel = plotG.append 'g'
    .attr
      class: 'label'

  xLabel.append 'text'
    .text 'Median wage ($)'
    .attr
      transform: 'rotate(-90)'
      
  yLabel = plotG.append 'g'
    .attr
      class: 'label'

  yLabel.append 'text'
    .text 'Probability of automation'

  footnote = plotG.append 'g'
    .attr
      class: 'footnote'
  
  footnote.append 'text'
    .text '*Data recorded in 2011; wage shown in 2012 dollars'

  dataSource = plotG.append 'g'
    .attr
      class: 'data-source'
  
  dataSource.append 'text'
    .text 'Data: Frey and Osborne; Bureau of Labor Statistics'

  plotCirclesGroup = plotG.append 'g'
    .attr
      class: 'plot-circles-group'
    
  plotCircles = plotCirclesGroup.selectAll 'plot-circle'
    .data csv
    .enter().append 'circle'
    .attr({
      class: 'plot-circle'
      'data-category': (d) -> cleanCategoryName d.category
    })

  updateDimensions = () ->
    plotDiv = document.getElementById('plot-wrapper')
    width = plotDiv.clientWidth
    height = Math.min(500, Math.max(250, width / 1.5))
    w = width - margin.l - margin.r
    h = height - margin.t - margin.b
    x.range [0, w]
    y.range [h, 0]

    if w < 400
      z.range [3, 16]
    else
      z.range [4, 25]

    xAxis.scale x
    yAxis.scale y
    
    plotSvg.attr
      width: width
      height: height

    xAxisSvg.attr
        transform: "translate(0, #{h})"
      .call xAxis

    yAxisSvg.call yAxis
    plotSvg.call tooltip

    plotCircles.attr
      cx: (d) -> x(d.probability)
      cy: (d) -> y(d.annual_median)
      r: (d) -> z(d.total_employed)
    .on 'mouseover', mouseOn
    .on 'mouseout', mouseOff

    xLabel.attr
      transform: "translate(#{[15, 0]})"

    yLabel.attr
      transform: "translate(#{[w, h - 5]})"

    footnote.attr
      transform: "translate(#{[w, h + 55]})"

    dataSource.attr
      transform: "translate(#{[w, h + 35]})"

  d3.select window
    .on 'resize', throttle(updateDimensions, 100)

  updateDimensions()
