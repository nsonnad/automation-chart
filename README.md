##Gulp app template

Minimalist template for making single-page apps with
[Gulp.js](http://gulpjs.com/). Compiles [Jade](http://jade-lang.com/),
[Coffeescript](http://coffeescript.org/), and
[Stylus](http://learnboost.github.io/stylus/). Has livereload, concatenation via
[Browserify](http://browserify.org/), and minification.

Easy to use.

  `git clone git@github.com:nsonnad/gulp-app-template.git`
  `npm install`

Install all browser dependencies with `npm install`, and include them in your
browser code with `require('module')`. Run `gulp` to create a livereload-cabaple
server, or `gulp build` to build out your app.
