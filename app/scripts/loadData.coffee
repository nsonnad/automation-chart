d3 = require 'd3'
categories = require('./categories')
makeDivs = require('./makeDivs')

d3.csv 'data/final.csv', (csv) ->
  csv.forEach (d) ->
    d.annual_median = +d.annual_median
    d.total_employed = +d.total_employed
    d.probability = +d.probability
    d.category = d.category.replace ' Occupations', ''

  categories csv
  makeDivs csv
