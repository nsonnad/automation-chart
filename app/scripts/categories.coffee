d3 = require 'd3'
cleanCategoryName = require './cleanCategoryName'
interaction = require './interaction'

module.exports = (csv) ->
  categories = d3.set(csv.map (d) -> d.category).values().sort()

  categoryDivsWrapper = d3.select '#category-divs-wrapper'
  categoryDropdownWrapper = d3.select '#category-dropdown-wrapper'
  
  categoryDivs = categoryDivsWrapper.selectAll '.category-div'
    .append 'div'
    .data categories
    .enter().append 'div'
      .attr
        class: 'category-div'
        'data-category': (d) -> cleanCategoryName d
      .on 'mouseover', interaction.buttonsHover
      .on 'mouseout', interaction.buttonsMouseout
      .on 'click', interaction.buttonsClick

  categoryDivs.append 'p'
    .html (d) -> d

  categoryDropdown = categoryDropdownWrapper.append 'select'
    .attr
      class: 'category-dropdown-select'

  categoryDropdown.append 'option'
    .html 'Select a Job Category'
    .attr 'selected', 'selected'

  categoryDropdownOpts = categoryDropdown.selectAll '.category-dropdown-option'
    .data categories
  .enter().append 'option'
    .attr
      class: 'category-dropdown-option'
      'data-category': (d) -> cleanCategoryName d
    .html (d) -> d

  categoryDropdown
    .on 'change', interaction.dropdown
