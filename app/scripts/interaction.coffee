d3 = require 'd3'
cleanCategoryName = require './cleanCategoryName'

d3.selection.prototype.moveToFront = () ->
  return this.each () ->
    this.parentNode.appendChild(this)

module.exports = {
  buttonsHover: () ->
    d3self = d3.select this
    console.log d3self
    currentCategory = d3self.attr 'data-category'

    d3.selectAll '.category-div'
      .classed 'active-hover', false

    d3.selectAll ".category-div[data-category=#{currentCategory}]"
      .classed 'active-hover', true

    d3.selectAll '.plot-circle'
      .classed 'active-hover', false

    d3.selectAll ".plot-circle[data-category=#{currentCategory}]"
      .classed 'active-hover', true
      
  buttonsMouseout: () ->
    d3.selectAll '.category-div'
      .classed 'active-hover', false

    d3.selectAll '.plot-circle'
      .classed 'active-hover', false

  buttonsClick: () ->
    d3self = d3.select this
    currentCategory = d3self.attr 'data-category'

    if not d3self.classed 'active-click'
      d3.selectAll '.category-div'
        .classed 'active-click', false

      d3self.classed 'active-click', true

      d3.selectAll '.plot-circle'
        .classed 'active-click', false

      d3.selectAll ".plot-circle[data-category=#{currentCategory}]"
        .classed 'active-click', true
        .moveToFront()
    else
      d3.selectAll '.category-div'
        .classed 'active-click', false

      d3.selectAll '.plot-circle'
        .classed 'active-click', false

  dropdown: (d) ->
    val = d3.select '.category-dropdown-select'
      .property 'value'

    currentCategory= cleanCategoryName val

    d3.selectAll '.plot-circle'
      .classed 'active-click', false

    d3.selectAll ".plot-circle[data-category=#{currentCategory}]"
      .classed 'active-click', true
      .moveToFront()
    
  ios: navigator.userAgent.match(/(iPod|iPhone|iPad)/)
}
